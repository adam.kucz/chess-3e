package pl.iilopoznan.adamkucz.chess3e.board;
// software design principles in OOP
// - abstraction
// - divide & conquer / modularity
//   in OOP divide & conquer is also called
//   "modularity" because we are dividing programs
//   into largely self-contained classes, which we can call modules
// - encapsulation
//   keeping relevant data and functions together and hiding the irrelevant to the user
//   details from their view (achieved via "private" keyword in Java)

import pl.iilopoznan.adamkucz.chess3e.Color;
import pl.iilopoznan.adamkucz.chess3e.pieces.*;

import java.util.Arrays;
import java.util.EnumSet;
import java.util.List;
import java.util.function.Function;
import java.util.stream.IntStream;

// modifiers in Java
// public/private - access modifiers which control where we can use attribute/method
//   private means only within that class
//   public means anywhere
// static - means that we don't need an object to already exist to call the method/use field,
//        so we cannot use non-static fields/methods from a static one
// final - means attribute cannot be changed after it's set (given a value)
public class Board {
    private final Piece[] fields = new Piece[64];

    // initialisation block
    {
        placePieceAt(new Rook(Color.WHITE), 0, 0);
        placePieceAt(new Rook(Color.WHITE), 7, 0);
        placePieceAt(new Rook(Color.BLACK), 0, 7);
        placePieceAt(new Rook(Color.BLACK), 7, 7);

        placePieceAt(new Knight(Color.WHITE), 1, 0);
        placePieceAt(new Knight(Color.WHITE), 6, 0);
        placePieceAt(new Knight(Color.BLACK), 1, 7);
        placePieceAt(new Knight(Color.BLACK), 6, 7);

        placePieceAt(new Bishop(Color.WHITE), 2, 0);
        placePieceAt(new Bishop(Color.WHITE), 5, 0);
        placePieceAt(new Bishop(Color.BLACK), 2, 7);
        placePieceAt(new Bishop(Color.BLACK), 5, 7);

        placePieceAt(new Queen(Color.WHITE), 3, 0);
        placePieceAt(new Queen(Color.BLACK), 3, 7);

        placePieceAt(new King(Color.WHITE), 4, 0);
        placePieceAt(new King(Color.BLACK), 4, 7);
    }

    public Board(Function<Position, Piece> whitePromote, Function<Position, Piece> blackPromote) {
        for (int column = 0; column < 8; column++){
            placePieceAt(new Pawn(Color.WHITE, whitePromote), column, 1);
            placePieceAt(new Pawn(Color.BLACK, blackPromote), column, 6);
        }
    }

    // extra: a bit more "clever" initialisation
    // important: this uses a lot of elements we will not talk about and is only provided as an extra
    // the argument is there just so that this is not treated as the default (argument-less) constructor
    private Board(Function<Position, Piece> whitePromote, Function<Position, Piece> blackPromote, boolean smart) {
        List<Function<Color,Piece>> pieceFactories = Arrays.asList(
                Rook::new, Knight::new, Bishop::new, Queen::new, King::new, Bishop::new, Knight::new, Rook::new
        );
        IntStream.range(0, 8).forEach(column -> {
            placePieceAt(pieceFactories.get(column).apply(Color.WHITE), column, 0);
            placePieceAt(new Pawn(Color.WHITE, whitePromote), column, 1);
            placePieceAt(pieceFactories.get(column).apply(Color.BLACK), column, 7);
            placePieceAt(new Pawn(Color.BLACK, blackPromote), column, 6);
        });
    }

    public Board(Board board) {
        for (int i = 0; i < fields.length; i++) {
            Piece p = board.fields[i];
            if (p != null) {
                fields[i] = board.fields[i].copy();
            } else {
                fields[i] = null;
            }

        }
    }

    private int toIndex(int x, int y) {
        return x + 8 * y;
    }

    private int toIndex(Position pos) {
        return toIndex(pos.getX(), pos.getY());
    }

    private Position fromIndex(int i) {
        return new Position(i % 8, i / 8);
    }

    /**
     * Returns the piece at (x, y).
     *
     * @param x x (file) position, 0 to 7
     * @param y y (rank) position, 0 to 7
     * @return piece at (x, y)
     */
    public Piece getPieceAt(int x, int y) {
        return fields[toIndex(x, y)];
    }

    /**
     * Returns the piece at (x, y).
     *
     * @param position (x, y) = (file, rank) pair of 0-7 ints for position
     * @return piece at (x, y)
     */
    public Piece getPieceAt(Position position) {
        // this is called "polymorphism" (many forms)
        // we have multiple functions called getPieceAt
        // but with different signature, Java decides which one to call
        // based on the arguments passed
        // signature is the name of the function + its argument types and number
        return getPieceAt(position.getX(), position.getY());
    }

    /**
     * This checks if there is a piece of opposite color at position pos
     * @return whether pos contains a (possibly phantom) piece of opposite color
      */
    public boolean hasEnemyPiece(Position pos, Color ownColor) {
        Piece piece = getPieceAt(pos);
        // Piece is a class type, so remember, it will refer to the piece
        // by its address, one feature of that is that this address
        // can be empty, that is denoted by "null" in Java (None in Python)
        if (piece != null) {
            return piece.getColor() != ownColor;
        }
        return false;
    }

    public Piece removePieceAt(int x, int y) {
        Piece previous = fields[x + y*8];
        fields[x + y*8] = null;
        return previous;
    }

    public Piece removePieceAt(Position target) {
        return removePieceAt(target.getX(), target.getY());
    }

    public void placePieceAt(Piece piece, Position pos) {
        placePieceAt(piece, pos.getX(), pos.getY());
    }

    private void placePieceAt(Piece piece, int x, int y) {
        fields[toIndex(x, y)] = piece;
    }

    /**
     *
     * @param pos position to check
     * @return whether the position is empty (phantom pieces count as empty)
     */
    public boolean isEmpty(Position pos) {
        Piece piece = getPieceAt(pos); 
        return piece == null || piece.isPhantom();
    }

    public boolean isInCheck(Position pos, Color endangeredColor) {
        for (int i = 0; i < fields.length; i++) {
            Piece piece = fields[i];
            Position piecePos = fromIndex(i);
            if (piece != null &&
                    piece.getColor() != endangeredColor &&
                    piece.canTakeAt(new Move(piecePos, pos), this)) {
                return true;
            }
        }
        return false;
    }


    private boolean willKingBeInCheck(Color kingColorToCheck, Move move) {
        // we do phantom moves which will be recovered from later
        // important: this does not inform pieces that they were (phantom) moved,
        // so no change to piece state takes place
        Piece removed = removePieceAt(move.getDst()); // the old piece that could potentially be taken
        Piece movedPiece = removePieceAt(move.getSrc()); // the piece being moved
        placePieceAt(movedPiece, move.getDst());
        Position kingPos = getKingPosition(kingColorToCheck);
        boolean answer = isInCheck(kingPos, kingColorToCheck);
        // undo phantom moves
        placePieceAt(removed, move.getDst());
        placePieceAt(movedPiece, move.getSrc());
        return answer;
    }

    public Position getKingPosition(Color color) {
        for (int i = 0; i < fields.length; i++) {
            Piece piece = fields[i];
            if (piece instanceof King && piece.getColor() == color) {
                return fromIndex(i);
            }
        }
        return null;
    }

    /**
     * Checks if the path along the given direction is clear and the move corresponds to a move in that direction.
     * Does not check the first and last tiles of the move themselves.
     *
     * @param move move to verify
     * @param direction movement direction
     * @return true if move was successfully made, false otherwise
     */
    public boolean isPathInLineClear(Move move, Direction direction) {
        if (!move.isInDirection(direction)) {
            return false;
        }
        int n = move.howFarInDirection(direction);
        for (int i = 1; i < n; i++) {
            if (!isEmpty(move.getSrc().shift(direction.mult(i)))) {
                return false;
            }
        }
        return true;
    }

    /**
     * Returns current state of the board given that it's move of the given player.
     *
     * @param currentPlayer color of the next player to move
     * @return BoardState.ONGOING, BoardState.DRAW, BoardState.WHITE_WON, or BoardState.BLACK_WON, depending on positions of pieces
     */
    public BoardState getState(Color currentPlayer) {
        // go through all of the currentPlayer pieces and check if any of them have legal moves
        // if so, the game is ongoing
        for (int i = 0; i < fields.length; i++) {
            Piece p = fields[i];
            if (p != null && p.getColor() == currentPlayer) {
                for (int j = 0; j < fields.length; j++) {
                    Move move = new Move(fromIndex(i), fromIndex(j));
                    if (p.canMoveTo(move, this) && !willKingBeInCheck(currentPlayer, move)) {
                        return BoardState.ONGOING;
                    }
                }
            }
        }
        // otherwise, if the king of current player is not in check, then stalemate, otherwise the other player wins
        Position kingPos = getKingPosition(currentPlayer);
        if (!isInCheck(kingPos, currentPlayer)) {
            return BoardState.DRAW;
        }
        if (currentPlayer == Color.BLACK) {
            return BoardState.WHITE_WON;
        } else {
            return BoardState.BLACK_WON;
        }
    }

    /**
     *
     * @param currentPlayerColor whether the move to be played is player 1 (white) move
     * @param move the move to be played
     * @return true if move was successful, false otherwise
     */
    public EnumSet<MoveType> play(Color currentPlayerColor, Move move) {
        Piece piece = getPieceAt(move.getSrc());
        if (piece == null) {
            return EnumSet.noneOf(MoveType.class);
        }
        EnumSet<MoveType> result = EnumSet.noneOf(MoveType.class);
        if (piece instanceof Pawn) {
            result.add(MoveType.PAWN);
        }
        if (piece.getColor() != currentPlayerColor) {
            return result;
        }
        boolean locallyPossible = piece.canMoveTo(move, this);
        if (!locallyPossible) {
            return result;
        }
        // dealing with check
        if (willKingBeInCheck(currentPlayerColor, move)) {
            return result;
        }
        result.add(MoveType.LEGAL);
        removePieceAt(move.getSrc());
        Piece taken = removePieceAt(move.getDst());
        if (taken != null) {
            taken.registerTaken(this);
            result.add(MoveType.CAPTURE);
        }
        placePieceAt(piece, move.getDst());
        clearPhantoms();
        piece.registerMove(move, this);
        return result;
    }

    private void clearPhantoms() {
        for (int i = 0; i < fields.length; i++) {
            Piece piece = fields[i];
            if (piece != null && piece.isPhantom()) {
                fields[i] = null;
            }
        }
    }

    /**
     * Shows the board to the screen
     */
    public void show() {
        showColumnNames();
        for (int rank = 7; rank >= 0; rank--) {
            for (int i = 0; i<3; i++) {
                if (i == 1) {
                    System.out.print(rank + 1);
                } else {
                    System.out.print(" ");
                }
                System.out.print(" ");
                for (int column = 0; column < 8; column++) {
                    Piece piece = getPieceAt(column, rank);
                    String background = (rank + column) % 2 == 0 ? "█" : " ";
                    String center = (piece == null || piece.isPhantom()) ? background :
                            String.valueOf(piece.oneCharRepresentation());
                    String text;
                    if (i != 1) {
                        text = background + background + background;
                    } else {
                        text = background + center + background;
                    }
                    System.out.print(text);
                }
                System.out.print(" ");
                if (i == 1) {
                    System.out.println(rank + 1);
                } else {
                    System.out.println(" ");
                }
            }
        }
        showColumnNames();
    }

    private static void showColumnNames() {
        System.out.print("   ");
        for (char column = 'A'; column <= 'H'; column++) {
            System.out.print(column + "  ");
        }
        System.out.println();
    }

    public boolean isValid(Position pos) {
        return 0 <= pos.getX() && pos.getX() < 8 && 0 <= pos.getY() && pos.getY() < 8;
    }

    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Board)) {
            return false;
        }
        Board otherBoard = (Board) other;
        return Arrays.equals(this.fields, otherBoard.fields);
    }
}
