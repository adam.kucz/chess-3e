package pl.iilopoznan.adamkucz.chess3e.board;

public class Direction {
    public static final Direction LEFT = new Direction(-1, 0);
    public static final Direction RIGHT = new Direction(1, 0);
    public static final Direction FORWARD = new Direction(0, 1);
    public static final Direction BACK = new Direction(0, -1);
    private final int dx;
    private final int dy;


    public Direction(int dx, int dy) {
        this.dx = dx;
        this.dy = dy;
    }

    public int getDx() {
        return dx;
    }

    public int getDy() {
        return dy;
    }

    public Direction mult(int i) {
        return new Direction(dx * i, dy * i);
    }
}
