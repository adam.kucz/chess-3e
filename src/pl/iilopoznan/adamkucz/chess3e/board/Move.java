package pl.iilopoznan.adamkucz.chess3e.board;

public class Move {
    private final Position src;

    private final Position dst;

    public int getxSrc() {
        return getSrc().getX();
    }

    public int getySrc() {
        return getSrc().getY();
    }

    public int getxDst() {
        return getDst().getX();
    }

    public int getyDst() {
        return getDst().getY();
    }

    public Direction getDirection() {
        return new Direction(getDx(), getDy());
    }

    public int getDx() {
        return getDst().getX() - getSrc().getX();
    }

    public int getDy() {
        return getDst().getY() - getSrc().getY();
    }

    public Position getSrc() {
        return src;
    }

    public Position getDst() {
        return dst;
    }

    public Move(String sourcePos, String destinationPos) {
        this(new Position(sourcePos), new Position(destinationPos));
    }

    public Move(Position src, Position dst) {
        this.src = src;
        this.dst = dst;
    }

    /**
     *
     * @return taxicab distance travelled in this move.
     */
    public int distance() {
        return Math.abs(getxDst() - getxSrc()) + Math.abs(getyDst() - getyDst());
    }

    public boolean isInDirection(Direction dir) {
        if (dir.getDx() == 0) {
            if (this.getDx() != 0) {
                return false;
            } else {
                return (dir.getDy() == 1 && this.getDy() > 0) || (dir.getDy() == -1 && this.getDy() <= 0);
            }
        } else {
            if ((dir.getDx() == 1 && this.getDx() > 0) || (dir.getDx() == -1 && this.getDx() < 0)) {
                int n = this.getDx() / dir.getDx();
                return this.getDy() == n * dir.getDy();
            } else {
                return false;
            }
        }
    }

    /**
     * Returns the number of tiles a new position is from the old one in the direction [xDir, yDir].
     * If the direction is incorrect, returns 0.
     *
     * @return 0 if the move is not in the given direction, otherwise n such that getDst() == getSrc().shift(dir.mult(i))
     */
    public int howFarInDirection(Direction dir) {
        if (dir.getDx() == 0) {
            if (this.getDx() != 0 || dir.getDy() == 0) {
                return 0;
            }
            return this.getDy() / dir.getDy();
        } else {
            int n = this.getDx() / dir.getDx();
            if (!(this.getDy() == n * dir.getDy())) {
                return 0;
            }
            return n;
        }
    }
}
