package pl.iilopoznan.adamkucz.chess3e.board;

public enum MoveType {
    CAPTURE, PAWN, LEGAL;
}
