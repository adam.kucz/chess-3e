package pl.iilopoznan.adamkucz.chess3e.board;

public class Position {
    private final int x;
    private final int y;

    public Position(int x, int y) {
        this.x = x;
        this.y = y;
    }

    /**
     * Converts human-readable notation to a valid board position
     *
     * @param humanNotation the human-readable chess notation for a position
     * @see Position#toHumanNotation
     */
    public Position(String humanNotation) {
        if (humanNotation.length() != 2) {
            String msg = "Human notation for chess positions must be 2 characters long! Got: " + humanNotation;
            throw new IllegalArgumentException(msg);
        }
        x = (int) Character.toUpperCase(humanNotation.charAt(0)) - 65;
        y = Character.getNumericValue(humanNotation.charAt(1) - 1);
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    @Override
    public String toString() {
        return "[" + (x+1) + ", " + (y + 1) + "]";
    }

    /**
     * @return A representation of the position in human-readable format.
     */
    public String toHumanNotation() {
        // char in Java is a type for single characters, these can be encoded with numbers
        // search "ASCII table" for the codes
        char fst = (char) (x + 65);
        String snd = String.valueOf(y + 1);
        return fst + snd;
    }

    public static void main(String[] args) {
        System.out.println(new Position("A1"));
        System.out.println(new Position("C7"));
        System.out.println(new Position(0, 0).toHumanNotation());
        System.out.println(new Position(2, 6).toHumanNotation());
    }

    public Position shift(int x, int y) {
        return new Position(getX() + x, getY() + y);
    }

    public Position shift(Direction dir) {
        return shift(dir.getDx(), dir.getDy());
    }

    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Position)) {
            return false;
        }
        Position otherPos = (Position) other;
        return this.x == otherPos.x && this.y == otherPos.y;
    }
}
