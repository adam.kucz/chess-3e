package pl.iilopoznan.adamkucz.chess3e.board;

// represents whether the game is finished/someone won/etc.
public enum BoardState {
    ONGOING, WHITE_WON, BLACK_WON, DRAW;
}
