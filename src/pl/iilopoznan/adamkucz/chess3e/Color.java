package pl.iilopoznan.adamkucz.chess3e;

// an enumeration, a special kind of class
// which allows us to express a finite number of possibilities
public enum Color {
    BLACK, WHITE;

    public Color other() {
        return this == WHITE ? BLACK : WHITE;
    }
}
