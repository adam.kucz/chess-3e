package pl.iilopoznan.adamkucz.chess3e.player;

import pl.iilopoznan.adamkucz.chess3e.Color;

import java.util.ArrayList;
import java.util.List;

public class RecordingPlayer extends Player {
    private final List<String> records = new ArrayList<>();

    public RecordingPlayer(Color color) {
        super(color);
    }

    @Override
    public String requestString(String msg) {
        String result = super.requestString(msg);
        if (result.equals("show")) {
            System.out.println(this);
        } else {
            records.add(result);
        }
        return result;
    }

    @Override
    public char requestChar(String msg) {
        char result = super.requestChar(msg);
        records.add(String.valueOf(result));
        return result;
    }

    @Override
    public String toString() {
        return super.toString() + records;
    }
}
