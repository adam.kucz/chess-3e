package pl.iilopoznan.adamkucz.chess3e.player;

import pl.iilopoznan.adamkucz.chess3e.Color;
import pl.iilopoznan.adamkucz.chess3e.IBIO;
import pl.iilopoznan.adamkucz.chess3e.board.Board;
import pl.iilopoznan.adamkucz.chess3e.board.Move;
import pl.iilopoznan.adamkucz.chess3e.board.MoveType;
import pl.iilopoznan.adamkucz.chess3e.board.Position;
import pl.iilopoznan.adamkucz.chess3e.pieces.*;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

public class Player {
    private final Color color;

    public Player(Color color) {
        this.color = color;
    }

    public EnumSet<MoveType> makeMove(Board board) {
        System.out.println("Turn of the " + color + " player.");
        EnumSet<MoveType> moveType = board.play(color, getUncheckedMove(board));
        while (!moveType.contains(MoveType.LEGAL)) {
            System.out.println("Invalid move, try again!");
            moveType = board.play(color, getUncheckedMove(board));
        }
        return moveType;
    }

    private Move getUncheckedMove(Board board) {
        Position sourcePos = getExistingPos(board,
                "Select your piece: ",
                "Invalid chess field, select valid piece: ");
        Position destinationPos = getExistingPos(board,
                "Select destination for the piece: ",
                "Invalid chess field, select existing destination for the piece: ");
        return new Move(sourcePos, destinationPos);
    }

    private Position getExistingPos(Board board, String initialMsg, String invalidMsg) {
        String posString = requestString(initialMsg);
        boolean valid;
        Position pos = null;
        try {
            pos = new Position(posString);
            valid = board.isValid(pos);
        } catch (IllegalArgumentException e) {
            valid = false;
        }
        while (!valid) {
            posString = requestString(invalidMsg);
            try {
                pos = new Position(posString);
                valid = board.isValid(pos);
            } catch (IllegalArgumentException e) {
                valid = false;
            }
        }
        return pos;
    }

    protected String requestString(String msg) {
        return IBIO.input(msg);
    }

    protected char requestChar(String msg) {
        return IBIO.inputChar(msg);
    }

    @Override
    public String toString() {
        return color + " player";
    }

    private final Map<Character, Function<Color, Piece>> promotionOptions = createPromotionMap();
    private static Map<Character, Function<Color, Piece>> createPromotionMap() {
        Map<Character, Function<Color, Piece>> promotionMap = new HashMap<>();
        promotionMap.put('r', Rook::new);
        promotionMap.put('k', Knight::new);
        promotionMap.put('b', Bishop::new);
        promotionMap.put('q', Queen::new);
        return promotionMap;
    }

    public Piece choosePromotion(Position position) {
        System.out.println("Congratulations, the pawn at " + position.toHumanNotation() + " is being promoted!");
        char choice = requestChar("Choose a piece to be promoted into [r/k/b/q]: ");
        Function<Color, Piece> constructor = promotionOptions.get(choice);
        while (constructor == null) {
            choice = requestChar("Invalid choice, the options are [r/k/b/q]. Choose again: ");
            constructor = promotionOptions.get(choice);
        }
        return constructor.apply(color);
    }
}
