package pl.iilopoznan.adamkucz.chess3e.player;

import pl.iilopoznan.adamkucz.chess3e.Color;

public class RecordedPlayer extends Player {
    private final String[] records;
    private int currentString = 0;

    public RecordedPlayer(String[] records, Color color) {
        super(color);
        this.records = records;
    }

    @Override
    public String requestString(String msg) {
        if (currentString < records.length) {
            String response = records[currentString];
            currentString++;
            return response;
        } else {
            return super.requestString(msg);
        }
    }

    @Override
    public char requestChar(String msg) {
        if (currentString < records.length) {
            String response = records[currentString];
            if (response.length() != 1) {
                throw new IllegalArgumentException("Requested a char when the string " + response + " was recorded!");
            }
            currentString++;
            return response.charAt(0);
        } else {
            return super.requestChar(msg);
        }
    }
}
