package pl.iilopoznan.adamkucz.chess3e;

import pl.iilopoznan.adamkucz.chess3e.board.Board;

public class GameState {
    public final Board board;
    public final Color nextMove;

    public GameState(Board board, Color nextMove) {
        this.board = new Board(board);
        this.nextMove = nextMove;
    }

    @Override
    public boolean equals(Object other) {
        if (!(other instanceof  GameState)) {
            return false;
        }
        GameState otherState = (GameState) other;
        return this.nextMove == otherState.nextMove && this.board.equals(otherState.board);
    }

}
