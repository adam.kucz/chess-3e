package pl.iilopoznan.adamkucz.chess3e.pieces;

import pl.iilopoznan.adamkucz.chess3e.Color;
import pl.iilopoznan.adamkucz.chess3e.board.Board;
import pl.iilopoznan.adamkucz.chess3e.board.Direction;
import pl.iilopoznan.adamkucz.chess3e.board.Move;
import pl.iilopoznan.adamkucz.chess3e.board.Position;

public class Rook extends DirectionalPiece {
    private static final Direction[] directions = {
            new Direction(1, 0),
            new Direction(0, 1),
            new Direction(0, -1),
            new Direction(-1, 0)
    };

    public Rook(Color color) {
        super(color);
    }

    public Rook(Rook other) {
        super(other);
    }

    @Override
    protected Direction[] getDirections() {
        return directions;
    }

    @Override
    public char symbol() {
        return 'r';
    }

    @Override
    public Rook copy() {
        return new Rook(this);
    }
}
