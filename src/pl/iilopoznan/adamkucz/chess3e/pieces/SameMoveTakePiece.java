package pl.iilopoznan.adamkucz.chess3e.pieces;

import pl.iilopoznan.adamkucz.chess3e.Color;
import pl.iilopoznan.adamkucz.chess3e.board.Board;
import pl.iilopoznan.adamkucz.chess3e.board.Move;

public abstract class SameMoveTakePiece extends Piece {

    protected SameMoveTakePiece(Color color) {
        super(color);
    }

    protected SameMoveTakePiece(SameMoveTakePiece other) {
        super(other);
    }

    @Override
    public boolean canMoveToEmptySpace(Move move, Board board) {
        return isPathClear(move, board);
    }

    @Override
    public boolean canTakeAt(Move move, Board board) {
        return canMoveToEmptySpace(move, board);
    }

    /**
     * Returns whether the new position is a valid position to move to, except it ignores what is in that position.
     *
     * @param move the move representing where the path should lead
     * @param board current board state
     * @return true if [newX, newY] could be moved to if it was empty, false otherwise.
     */
    public abstract boolean isPathClear(Move move, Board board);
    // abstract means that this method/class is not defined here
    // and you cannot call it for this superclass directly
    // all subclasses that you want to have will need to define their own versions of it
}
