package pl.iilopoznan.adamkucz.chess3e.pieces;

import pl.iilopoznan.adamkucz.chess3e.Color;
import pl.iilopoznan.adamkucz.chess3e.board.Direction;

public class Bishop extends DirectionalPiece {
    private static final Direction[] directions = {
            new Direction(1,  1),
            new Direction(-1,  1),
            new Direction( 1, -1),
            new Direction(-1, -1)
    };
    public Bishop(Color color) {
        super(color);
    }

    public Bishop(Bishop other) {
        super(other);
    }

    @Override
    protected Direction[] getDirections() {
        return directions;
    }

    @Override
    public char symbol() {
        return 'b';
    }

    @Override
    public Bishop copy() {
        return new Bishop(this);
    }
}
