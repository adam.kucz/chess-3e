package pl.iilopoznan.adamkucz.chess3e.pieces;

import pl.iilopoznan.adamkucz.chess3e.Color;
import pl.iilopoznan.adamkucz.chess3e.board.Board;
import pl.iilopoznan.adamkucz.chess3e.board.Move;
import pl.iilopoznan.adamkucz.chess3e.board.Position;

import java.util.function.Function;

public abstract class Piece {
    // for now, this will be just a pawn
    private boolean hasMoved = false;
    // "final" denotes a constant, so once it's set, it can never be changed
    // new access modifier: "protected" means that the class and all the subclasses based on it
    // can access it
    protected final Color color;
    // the piece position is already stored in the board, so let's not repeat that information
    // repeats like that easily lead to inconsistencies and bugs
    // protected int x;
    // protected int y;

    // "accessor" method or "getter"
    // method the only purpose of which is to provide access to an attribute/field
    // public int getX() {
    //    return x;
    //}

    //public int getY() {
    //    return y;
    //}

    // for completeness
    // the other kind of special method is called a "setter", "mutator" in IB
    // this allows just for setting a new value to a field
    /*
    public void setX(int newValue) {
        if (newValue >= 0 && newValue < 8) {
            x = newValue;
        }
    }
     */

    protected Piece(Color color) {
        this.color = color;
    }

    protected Piece(Piece other) {
        this.color = other.color;
        this.hasMoved = other.hasMoved;
    }

    /**
     * Checks if a move is a valid move for this piece. Never performs any actual moves.
     *
     * @param move the move to check
     * @return true if move was successful
     */
    public boolean canMoveTo(Move move, Board board) {
        if (canMoveToEmptySpace(move, board) && board.isEmpty(move.getDst())) {
            return true;
        } else if (canTakeAt(move, board) && board.hasEnemyPiece(move.getDst(), getColor())) {
            return true;
        }
        return false;
    }

    /**
     * Common element for both checks and moves. This will allow to hypothetically verify that a piece
     * would be able to take enemy piece at some location without actually performing the action.
     *
     * @return true is this piece could take enemy piece at position (newX, newY), false otherwise
     */
    public abstract boolean canTakeAt(Move move, Board board);

    /**
     * Basic element for moves. This will allow to hypothetically verify that a piece
     * would be able to move to some location without actually performing the action.
     *
     * @return true if this piece could move to an empty (newX, newY)
     */
    public abstract boolean canMoveToEmptySpace(Move move, Board board);

    // this method (which just returns a field)
    // is called a getter/accessor
    // advantage to using it: we can disallow modification of the field
    // (also: if we decide to change isWhite to isBlack in the future, this
    //    change can be restricted to only the pl.iilopoznan.adamkucz.chess3e.pieces.Piece class)
    public Color getColor() {
        return color;
    }

    public boolean isInInitialPosition() {
        return !hasMoved;
    }

    /**
     * Move to a new position given as an array
     *
     * @param newPos 2 element int array representing the new position
     * @param board current board state
     * @return true if move was successful
     */
    public final boolean canMoveTo(Position oldPos, Position newPos, Board board) {
        return canMoveTo(new Move(oldPos, newPos), board);
    }

    // this (having multiple methods with the same name)
    // is called polymorphism (overloading)
    // and the compiler/interpreter will decide which one to call based
    // on the signature, i.e. name + argument number and types
    public  final boolean canMoveTo(String oldPos, String newPos, Board board) {
        return canMoveTo(new Position(oldPos), new Position(newPos), board);
    }

    public void registerMove(Move move, Board board) {
        hasMoved = true;
    }

    public void registerTaken(Board board) {};

    /**
     * Phantom pieces are pieces which are placed purely for purposes of analysing moves of other pieces more easily.
     * They are ignored by most pieces and are usually deleted shortly after creation.
     *
     * @return whether this piece is a phantom piece
     */
    public boolean isPhantom() {
        return false;
    }

    /**
     * Returns a single character representation of the piece;
     *
     * @return a single character representing the piece
     */
    public abstract char symbol();

    public char oneCharRepresentation() {
        return getColor() == Color.WHITE ? Character.toUpperCase(symbol()) : Character.toLowerCase(symbol());
    }

    /**
     * Creates a copy of this piece.
     *
     * @return an independent copy of the piece with identical parameters.
     */
    public abstract Piece copy();

    @Override
    public boolean equals(Object other) {
        if (other == null || !this.getClass().equals(other.getClass())) {
            return false;
        }
        Piece otherPiece = (Piece) other;
        return this.hasMoved == otherPiece.hasMoved && this.color == otherPiece.color;
    }
}