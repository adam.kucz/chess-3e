package pl.iilopoznan.adamkucz.chess3e.pieces;

import pl.iilopoznan.adamkucz.chess3e.Color;
import pl.iilopoznan.adamkucz.chess3e.board.Board;
import pl.iilopoznan.adamkucz.chess3e.board.Move;
import pl.iilopoznan.adamkucz.chess3e.board.Direction;

public abstract class DirectionalPiece extends SameMoveTakePiece {
    protected DirectionalPiece(Color color) {
        super(color);
    }

    protected DirectionalPiece(DirectionalPiece other) {
        super(other);
    }

    protected abstract Direction[] getDirections();

    @Override
    public boolean isPathClear(Move move, Board board) {
        Direction[] directions = getDirections();
        for (int i = 0; i < directions.length; i++) {
            if (board.isPathInLineClear(move, directions[i])) {
                return true;
            }
        }
        return false;
    }
}
