package pl.iilopoznan.adamkucz.chess3e.pieces;

import pl.iilopoznan.adamkucz.chess3e.Color;
import pl.iilopoznan.adamkucz.chess3e.board.Direction;

public class Queen extends DirectionalPiece {
    private static final Direction[] directions = {
            new Direction(1,  1),
            new Direction(1,  0),
            new Direction(1,  -1),
            new Direction(0,  1),
            new Direction(0,  -1),
            new Direction(-1,  1),
            new Direction( -1, 0),
            new Direction(-1, -1)
    };

    public Queen(Color color) {
        super(color);
    }

    public Queen(Queen other) {
        super(other);
    }

    @Override
    protected Direction[] getDirections() {
        return directions;
    }


    @Override
    public char symbol() {
        return 'q';
    }

    @Override
    public Queen copy() {
        return new Queen(this);
    }
}
