package pl.iilopoznan.adamkucz.chess3e.pieces;

import pl.iilopoznan.adamkucz.chess3e.Color;
import pl.iilopoznan.adamkucz.chess3e.board.Board;
import pl.iilopoznan.adamkucz.chess3e.board.Move;
import pl.iilopoznan.adamkucz.chess3e.board.Position;

public class PhantomPawn extends Pawn {
    private final Position originalPos;

    public PhantomPawn(Color color, Position originalPos) {
        super(color, p -> { throw new RuntimeException("Phantom pawns should never be promoted!"); });
        this.originalPos = originalPos;
    }

    public PhantomPawn(PhantomPawn other) {
        super(other);
        originalPos = other.originalPos;
    }

    @Override
    public boolean canTakeAt(Move move, Board board) {
        return false;
    }

    @Override
    public boolean canMoveToEmptySpace(Move move, Board board) {
        return false;
    }

    @Override
    public boolean isPhantom() {
        return true;
    }

    @Override
    public void registerTaken(Board board) {
        board.removePieceAt(originalPos).registerTaken(board);
    }

    @Override
    public PhantomPawn copy() {
        return new PhantomPawn(this);
    }

    @Override
    public boolean equals(Object other) {
        return super.equals(other) && this.originalPos.equals(((PhantomPawn) other).originalPos);
    }

    public Position getOriginalPos() {
        return originalPos;
    }
}
