package pl.iilopoznan.adamkucz.chess3e.pieces;

import pl.iilopoznan.adamkucz.chess3e.Color;
import pl.iilopoznan.adamkucz.chess3e.board.Board;
import pl.iilopoznan.adamkucz.chess3e.board.Move;

public abstract class SetMoveEightfoldSymmetryPiece extends SameMoveTakePiece {

    protected SetMoveEightfoldSymmetryPiece(Color color) {
        super(color);
    }

    protected SetMoveEightfoldSymmetryPiece(SetMoveEightfoldSymmetryPiece other) {
        super(other);
    }

    protected abstract int[] getBaseMoveChanges();

    @Override
    public boolean isPathClear(Move move, Board board) {
        int[] diff = getBaseMoveChanges();
        for (int i = 0; i < diff.length; i += 2) {
            int baseDx = diff[i];
            int baseDy = diff[i + 1];
            int dx = move.getDx();
            int dy = move.getDy();
            if ((dx == baseDx || dx == -baseDx) && (dy == baseDy || dy == -baseDy)) {
                return true;
            }
            if ((dx == baseDy || dx == -baseDy) && (dy == baseDx || dy == -baseDx)) {
                return true;
            }
        }
        return false;
    }
}
