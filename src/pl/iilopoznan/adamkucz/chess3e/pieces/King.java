package pl.iilopoznan.adamkucz.chess3e.pieces;

import pl.iilopoznan.adamkucz.chess3e.Color;
import pl.iilopoznan.adamkucz.chess3e.board.Board;
import pl.iilopoznan.adamkucz.chess3e.board.Direction;
import pl.iilopoznan.adamkucz.chess3e.board.Move;
import pl.iilopoznan.adamkucz.chess3e.board.Position;

public class King extends SetMoveEightfoldSymmetryPiece {

    private static final int[] baseMoves = new int[] {
            0, 1,
            1, 1
    };

    public King(Color color) {
        super(color);
    }

    public King(King other) {
        super(other);
    }

    protected int[] getBaseMoveChanges() {
        return baseMoves;
    }

    @Override
    public boolean canMoveTo(Move move, Board board) {
        if (super.canMoveTo(move, board)) {
            return true;
        }
        // castling, design choice: readability over efficiency
        Direction kingMoveDir = null;
        Position rookPos = null;
        if (move.howFarInDirection(Direction.RIGHT) == 2) {
            // kingside castling
            rookPos = new Position(7, move.getySrc());
            kingMoveDir = Direction.RIGHT;
        } else if (move.howFarInDirection(Direction.LEFT) == 2) {
            // queenside castling
            rookPos = new Position(0, move.getySrc());
            kingMoveDir = Direction.LEFT;
        }
        if (rookPos == null) {
            return false;
        }
        Piece rook = board.getPieceAt(rookPos);
        if (rook == null) {
            return false;
        }
        if (!isInInitialPosition() || !rook.isInInitialPosition()) {
            return false;
        }
        if (!board.isPathInLineClear(move, kingMoveDir)) {
            return false;
        }
        return !board.isInCheck(move.getSrc(), getColor()) &&
                !board.isInCheck(move.getSrc().shift(kingMoveDir), getColor()) &&
                !board.isInCheck(move.getDst(), getColor());
    }

    @Override
    public void registerMove(Move move, Board board) {
        Direction kingMoveDir = null;
        Position rookPos = null;
        if (move.howFarInDirection(Direction.RIGHT) == 2) {
            // kingside castling
            rookPos = new Position(7, move.getySrc());
            kingMoveDir = Direction.RIGHT;
        } else if (move.howFarInDirection(Direction.LEFT) == 2) {
            // queenside castling
            rookPos = new Position(0, move.getySrc());
            kingMoveDir = Direction.LEFT;
        }
        if (rookPos != null) {
            Piece rook = board.getPieceAt(rookPos);
            board.removePieceAt(rookPos);
            board.placePieceAt(rook, move.getSrc().shift(kingMoveDir));
            rook.registerMove(new Move(rookPos, move.getSrc().shift(kingMoveDir)), board);
        }
    }

    @Override
    public char symbol() {
        return 'a';
    }

    @Override
    public King copy() {
        return new King(this);
    }
}
