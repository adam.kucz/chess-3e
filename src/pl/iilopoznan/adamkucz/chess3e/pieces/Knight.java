package pl.iilopoznan.adamkucz.chess3e.pieces;

import pl.iilopoznan.adamkucz.chess3e.Color;

public class Knight extends SetMoveEightfoldSymmetryPiece {
    private static final int[] baseMoves = new int[] {
            1, 2
    };

    public Knight(Color color) {
        super(color);
    }

    public Knight(Knight other) {
        super(other);
    }

    protected int[] getBaseMoveChanges() {
        return baseMoves;
    }

    @Override
    public char symbol() {
        return 'k';
    }

    @Override
    public Knight copy() {
        return new Knight(this);
    }
}
