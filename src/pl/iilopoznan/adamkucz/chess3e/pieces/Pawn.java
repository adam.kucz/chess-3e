package pl.iilopoznan.adamkucz.chess3e.pieces;

import pl.iilopoznan.adamkucz.chess3e.Color;
import pl.iilopoznan.adamkucz.chess3e.board.Board;
import pl.iilopoznan.adamkucz.chess3e.board.Direction;
import pl.iilopoznan.adamkucz.chess3e.board.Move;
import pl.iilopoznan.adamkucz.chess3e.board.Position;

import java.util.function.Function;

public class Pawn extends Piece {

    private final Function<Position, Piece> promoteAt;

    public Pawn(Color color, Function<Position, Piece> promote) {
        // "super" is referring to the superclass, here: pl.iilopoznan.adamkucz.chess3e.pieces.Piece
        // and the brackets mean we are calling its constructor
        super(color);
        this.promoteAt = promote;
    }

    public Pawn(Pawn other) {
        super(other);
        promoteAt = other.promoteAt;
    }

    // "Override" means that this is replacing a method from the superclass
    // so if something tries to call a move(x, y, board) on a Pawn
    // it will always use the method from the most specific subclass
    // this is called "inheritance polymorphism" because the choice of method
    // to call is made based on the inheritance hierarchy
    @Override
    public boolean canMoveToEmptySpace(Move move, Board board) {
        int direction;
        if (color == Color.WHITE) {
            direction = 1;
        } else {
            direction = -1;
        }
        if (move.getDx() == 0 && move.getDy() == direction) {
            return true;
        }
        if (move.getDx() == 0 && move.getDy() == 2*direction && board.isEmpty(move.getSrc().shift(0, direction)) &&
                isInInitialPosition()) {
            return true;
        }
        return false;
    }

    @Override
    public boolean canTakeAt(Move move, Board board) {
        int direction;
        if (color == Color.WHITE) {
            direction = 1;
        } else {
            direction = -1;
        }
        if ((move.getDx() == -1 || move.getDx() == 1) && move.getDy() == direction) {
            return true;
        }
        return false;
    }

    @Override
    public void registerMove(Move move, Board board) {
        if (move.howFarInDirection(Direction.FORWARD) == 2) {
            board.placePieceAt(new PhantomPawn(getColor(), move.getDst()), move.getSrc().shift(Direction.FORWARD));
        } else if (move.howFarInDirection(Direction.BACK) == 2) {
            board.placePieceAt(new PhantomPawn(getColor(), move.getDst()), move.getSrc().shift(Direction.BACK));
        }
        if ((color == Color.WHITE && move.getyDst() == 7) || (color == Color.BLACK && move.getyDst() == 0)) {
            Piece promoted = promoteAt.apply(move.getDst());
            board.placePieceAt(promoted, move.getDst());
        }
    }

    @Override
    public char symbol() {
        return 'p';
    }

    @Override
    public Pawn copy() {
        return new Pawn(this);
    }
}
