package pl.iilopoznan.adamkucz.chess3e;

import pl.iilopoznan.adamkucz.chess3e.board.Board;
import pl.iilopoznan.adamkucz.chess3e.board.BoardState;
import pl.iilopoznan.adamkucz.chess3e.board.MoveType;
import pl.iilopoznan.adamkucz.chess3e.player.Player;
import pl.iilopoznan.adamkucz.chess3e.player.RecordedPlayer;
import pl.iilopoznan.adamkucz.chess3e.player.RecordingPlayer;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

import static pl.iilopoznan.adamkucz.chess3e.Color.BLACK;
import static pl.iilopoznan.adamkucz.chess3e.Color.WHITE;

public class Game {
    private final Player player1;
    private final Player player2;
    private Color nextMoveColor = WHITE;
    private final Board board;
    private final List<GameState> history = new ArrayList<>();
    private int idleMoveCounter = 0;

    public Game() {
        player1 = new Player(WHITE);
        player2 = new Player(BLACK);
        board = new Board(player1::choosePromotion, player2::choosePromotion);
    }

    public Game(String whiteRecord, String blackRecord) {
        player1 = new RecordedPlayer(whiteRecord.split(" "), WHITE);
        player2 = new RecordedPlayer(blackRecord.split(" "), BLACK);
        board = new Board(player1::choosePromotion, player2::choosePromotion);
    }

    public Game(boolean record) {
        if (record) {
            player1 = new RecordingPlayer(WHITE);
            player2 = new RecordingPlayer(BLACK);
        } else {
            player1 = new Player(WHITE);
            player2 = new Player(BLACK);
        }
        board = new Board(player1::choosePromotion, player2::choosePromotion);
    }

    private void play() {
        // showing the board
        // controlling who gets a turn, letting them move
        // showing checkmates
        // and continuing on if no checkmate is detected
        saveBoardState();
        while (board.getState(nextMoveColor) == BoardState.ONGOING && idleMoveCounter < 50 && !threefoldRepetition()) {
            board.show();
            EnumSet<MoveType> moveTypes;
            if (nextMoveColor == WHITE) {
                moveTypes = player1.makeMove(board);
            } else {
                moveTypes = player2.makeMove(board);
            }
            if (moveTypes.contains(MoveType.PAWN) || moveTypes.contains(MoveType.CAPTURE)) {
                idleMoveCounter = 0;
            } else {
                idleMoveCounter++;
            }
            nextMoveColor = nextMoveColor.other();
            saveBoardState();
        }
        board.show();
        switch (board.getState(nextMoveColor)) {
            case WHITE_WON:
                System.out.println(player1 + " won!");
                break;
            case BLACK_WON:
                System.out.println(player2 + " won!");
                break;
            default:
                System.out.println("It's a draw!");
                break;
        }
    }

    private boolean threefoldRepetition() {
        // TODO: identify bugs
        for (int i = 0; i < history.size(); i++) {
            int count = 1;
            for (int j = i+1; j < history.size(); j++) {
                if (history.get(i).equals(history.get(j))) {
                    count++;
                    System.out.println("Found " + count + " repetitions");
                    if (count >= 3) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private void saveBoardState() {
        history.add(new GameState(board, nextMoveColor));
    }

    public static void main(String[] args) {
        System.out.println("Choose run settings, the options are:");
        System.out.println("0. regular game");
        System.out.println("1. game that records the moves");
        System.out.println("2. pre-recorded game");
        int choice = IBIO.inputInt("Your choice: ");
        Game game;
        switch (choice) {
            case 1:
                game = new Game(true);
                break;
            case 2:
                System.out.println("Choose record from:");
                System.out.println("0. Fool's mate");
                System.out.println("1. Scholar's mate");
                System.out.println("2. En passant example");
                System.out.println("3. Unsuccessful en passant");
                System.out.println("4. Castling black");
                System.out.println("5. Castling white");
                System.out.println("6. White pawn promotion");
                System.out.println("7. Threefold repetition");
                int recordChoice = IBIO.inputInt("Your choice: ");
                switch (recordChoice) {
                    case 0:
                        game = new Game("F2 F3 G2 G4", "E7 E5 D8 H4"); // fool's mate
                        break;
                    case 1:
                        game = new Game("E2 E4 F1 C4 D1 H5 H5 F7", "E7 E5 B8 C6 G8 F6"); // scholar's mate
                        break;
                    case 2:
                        game = new Game("E2 E4 E4 E5 E5 F6", "A7 A6 F7 F5"); // en passant successful
                        break;
                    case 3:
                        game = new Game("E2 E4 E4 E5 A2 A3 E5 F6", "A7 A6 F7 F5 A6 A5"); // en passant too late
                        break;
                    case 4:
                        game = new Game("A2 A3 B2 B3 C2 C3 D2 D3 E2 E3 F2 F3 G2 G3 H2 H3",
                                "B8 A6 C7 C5 D7 D5 C8 G4 D8 A5 E8 C8"); // castling black
                        break;
                    case 5:
                        game = new Game("G1 F3 E2 E4 F1 B5 H1 E1 E1 H1 E1 G1", "E7 E5 D7 D6 G8 F6 C7 C6"); // castling white
                        break;
                    case 6:
                        game = new Game("A2 A4 B2 B4 B4 A5 A5 A6 A6 A7 A7 A8 q A8 D8",
                                "A7 A5 A8 A6 A6 H6 B8 C6 B7 B5 C8 A6 E8 D8"); // white promotion
                        break;
                    case 7:
                        game = new Game("B1 A3 A3 B1 B1 A3 A3 B1 B1 A3 A3 B1 B1 A3",
                                "B8 A6 A6 B8 B8 A6 A6 B8 B8 A6 A6 B8"); // white promotion
                        break;
                    default:
                        game = new Game();
                }
                break;
            default:
            case 0:
                game = new Game();
        }
        game.play();
    }
}
